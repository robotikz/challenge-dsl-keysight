const fs = require('fs');
const path = require('path');

let app = {};

let basePath = '';

function parseImports(filePath, indent = '    ') {
    const fileContent = fs.readFileSync(basePath + filePath, 'utf8');
    const importRegex = /import\s+(.+?);/g;

    let match;
    while ((match = importRegex.exec(fileContent))) {
        const importedPath = match[1];
        const fileName = path.basename(importedPath);
        console.log(indent + fileName);
        const subIndent = indent + '    ';
        parseImports(importedPath, subIndent);
    }
}

function main() {
    if (process.argv.length < 4) {
        console.log('Usage: node ./src/main.js ./examples/example1/ root.prog');
        return;
    }

    basePath = process.argv[2];
    const rootProgramName = process.argv[3];
    console.log(rootProgramName);
    app.parseImports(rootProgramName);
}

app = { parseImports, main, basePath }
app.main();



if (process.env['NODE_DEV'] == 'test') {
    module.exports = app;
}
