# Before start
run `npm install` from the root folder

# How to start it
from the root folder run

`npm start ./examples/example1/ root.prog`

to run without npm scripts

`node ./src/main.js ./examples/example1/ root.prog`


# How to test it
from the root folder run
`npm test`
