// import { exportsForTesting } from './main';

process.env['NODE_DEV'] = 'test';

const fs = require('fs');

const app = require('./main');



// Mocking fs.readFileSync
jest.mock('fs', () => ({
    readFileSync: jest.fn(),
}));


describe('main', () => {

    it('should handle missing arguments', () => {
        // Mock process.argv to simulate missing arguments
        const originalArgv = process.argv;
        process.argv = ['node', 'main.js'];

        const consoleSpy = jest.spyOn(console, 'log').mockImplementation();

        // Import and call the main function
        app.main();

        // Assertions
        expect(consoleSpy).toHaveBeenCalledWith('Usage: node ./src/main.js ./examples/example1/ root.prog');

        // Restore process.argv and console.log
        process.argv = originalArgv;
        jest.restoreAllMocks();
        jest.resetModules();
    });

    it('should parse imports', () => {
        // Mock process.argv to simulate valid arguments
        const originalArgv = process.argv;
        process.argv = ['node', './src/main.js', './examples/example1/', 'root.prog'];

        const parseImportsSpy = jest.spyOn(app, 'parseImports').mockImplementation();
        const consoleSpy = jest.spyOn(console, 'log').mockImplementation();

        // Import and call the main function
        app.main();

        // Assertions
        expect(consoleSpy).toHaveBeenCalledWith('root.prog');
        expect(parseImportsSpy).toHaveBeenCalled();
        expect(parseImportsSpy).toHaveBeenCalledWith('root.prog');

        // Restore process.argv and module mocks
        process.argv = originalArgv;
        jest.restoreAllMocks();
        jest.resetModules();
    });
});

describe('parseImports', () => {

    beforeEach(() => {
        jest.restoreAllMocks();
        jest.resetModules();
    })


    it('should parse imports with nested structure', () => {

        const consoleSpy = jest.spyOn(console, 'log').mockImplementation();
        fs.readFileSync.mockReturnValueOnce(`
            import library1.lib;
            import library2.lib;
            import library3.lib;
        `);

        app.parseImports('root.prog');

        expect(fs.readFileSync).toHaveBeenCalledWith('./examples/example1/root.prog', 'utf8');
        expect(fs.readFileSync).toHaveBeenCalledWith('./examples/example1/library1.lib', 'utf8');
        expect(fs.readFileSync).toHaveBeenCalledWith('./examples/example1/library2.lib', 'utf8');
        expect(fs.readFileSync).toHaveBeenCalledWith('./examples/example1/library3.lib', 'utf8');
        expect(consoleSpy).toHaveBeenCalledWith('    library1.lib');
        expect(consoleSpy).toHaveBeenCalledWith('    library2.lib');
        expect(consoleSpy).toHaveBeenCalledWith('    library3.lib');
    });

    it('should parse imports in subfolder', () => {
        fs.readFileSync.mockImplementation();
        const consoleSpy = jest.spyOn(console, 'log').mockImplementation();
        fs.readFileSync.mockReturnValueOnce(`
            import library1.lib;
            import path/to/library2.lib;
        `);

        app.parseImports('root.prog');

        expect(fs.readFileSync).toHaveBeenCalledWith('./examples/example1/root.prog', 'utf8');
        expect(fs.readFileSync).toHaveBeenCalledWith('./examples/example1/library1.lib', 'utf8');
        expect(fs.readFileSync).toHaveBeenCalledWith('./examples/example1/path/to/library2.lib', 'utf8');
        expect(consoleSpy).toHaveBeenCalledWith('    library1.lib');
        expect(consoleSpy).toHaveBeenCalledWith('    library2.lib');
    });

    it('should parse imports in import', () => {
        fs.readFileSync.mockImplementation();
        const consoleSpy = jest.spyOn(console, 'log').mockImplementation();
        fs.readFileSync.mockReturnValueOnce(`
            import library1.lib;
            import library2.lib;
        `);

        fs.readFileSync.mockImplementation((filePath) => {
            // This argument is the one passed to the function you are mocking
            if (filePath.includes('library2.lib')) {
                return 'import subfolder/library3.lib;'
            } else {
                return '';
            }
        });

        app.parseImports('root.prog');

        expect(fs.readFileSync).toHaveBeenCalledWith('./examples/example1/root.prog', 'utf8');
        expect(fs.readFileSync).toHaveBeenCalledWith('./examples/example1/library1.lib', 'utf8');
        expect(fs.readFileSync).toHaveBeenCalledWith('./examples/example1/library2.lib', 'utf8');
        expect(fs.readFileSync).toHaveBeenCalledWith('./examples/example1/subfolder/library3.lib', 'utf8');
        expect(consoleSpy).toHaveBeenCalledWith('    library1.lib');
        expect(consoleSpy).toHaveBeenCalledWith('    library2.lib');
        expect(consoleSpy).toHaveBeenCalledWith('        library3.lib');
    });
});
